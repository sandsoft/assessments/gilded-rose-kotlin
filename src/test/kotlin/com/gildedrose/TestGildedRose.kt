package com.gildedrose

import com.gildedrose.item.Item
import com.gildedrose.item.getUpdatableItem
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class TestGildedRose {

    @Test
    fun `test new item addition for Gilded Rose`() {
        val newItem = getUpdatableItem(Item("Conjured Mana Cake", 3, 6))
        var gildedRose = GildedRose(listOf(newItem))

        for (i in 0..2) {
            gildedRose = gildedRose.update()
        }

        assertThat(gildedRose.items.first().quality).isEqualTo(0)
        assertThat(gildedRose.items.first().sellIn).isEqualTo(0)
    }
}
