package com.gildedrose

import com.gildedrose.item.Item
import com.gildedrose.item.getUpdatableItem
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.*

class GoldenMasterTests {

    private val outContent = ByteArrayOutputStream()
    private val errContent = ByteArrayOutputStream()

    private val items = listOf(
        Item("+5 Dexterity Vest", 10, 20),
        Item("Aged Brie", 2, 0),
        Item("Elixir of the Mongoose", 5, 7),
        Item("Sulfuras, Hand of Ragnaros", 0, 80),
        Item("Sulfuras, Hand of Ragnaros", -1, 80),
        Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
        Item("Backstage passes to a TAFKAL80ETC concert", 10, 49),
        Item("Backstage passes to a TAFKAL80ETC concert", 5, 49),
        // this conjured item does not work properly yet
        // Item("Conjured Mana Cake", 3, 6),
    )
    private val updatableItems = items.map(::getUpdatableItem)

    private fun summarize(days: Int) {
        var gildedRose = GildedRose(items = updatableItems)

        for (i in 0 until days) {
            println("-------- day $i --------")
            println("name, sellIn, quality")

            gildedRose.items.forEach { println(it) }
            println()

            gildedRose = gildedRose.update()
        }
    }

    private fun readInput(name: String) = File("src/test/kotlin/com/gildedrose", "$name.txt").readText()

    @BeforeEach
    private fun beforeEach() {
        outContent.reset()
        errContent.reset()
        captureOut()
        captureErr()
    }

    private fun captureOut() {
        System.setOut(PrintStream(outContent))
    }

    private fun captureErr() {
        System.setErr(PrintStream(errContent))
    }

    private fun getOut(): String {
        System.setOut(PrintStream(FileOutputStream(FileDescriptor.out)))
        return outContent.toString().replace("\r".toRegex(), "")
    }

    @Test
    fun `golden master test 2 days`() {
        summarize(2)

        assertThat(getOut()).isEqualTo(readInput("gm2days_output"))
    }

    @Test
    fun `golden master test 5 days`() {
        summarize(5)

        assertThat(getOut()).isEqualTo(readInput("gm5days_output"))
    }

    @Test
    fun `golden master test 10 days`() {
        summarize(10)

        assertThat(getOut()).isEqualTo(readInput("gm10days_output"))
    }

    @Test
    fun `golden master test 15 days`() {
        summarize(15)

        assertThat(getOut()).isEqualTo(readInput("gm15days_output"))
    }
}
