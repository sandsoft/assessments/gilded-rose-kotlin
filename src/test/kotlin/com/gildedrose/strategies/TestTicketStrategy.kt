package com.gildedrose.strategies

import com.gildedrose.item.Item
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

class TestTicketStrategy {

    private val strategy = ::ticketStrategy

    @Test
    fun `update of item with expired sell date sets its value to 0`() {
        val item = Item("Backstage passes to a TAFKAL80ETC concert", -1, 20)

        val updatedItem = strategy(item)

        assertThat(updatedItem.quality).isEqualTo(0)
    }

    @ParameterizedTest(name = "Almost expired items with a sell date of {0} increase in quality by 3")
    @ValueSource(ints = [1, 2, 3, 4, 5])
    fun `update of non-expired item with sell date between 0 and 5 days increases its value by 3`(sellIn: Int) {
        val item = Item("Backstage passes to a TAFKAL80ETC concert", sellIn, 20)

        val updatedItem = strategy(item)

        assertThat(updatedItem.quality).isEqualTo(item.quality + 3)
    }

    @ParameterizedTest(name = "Far from expired items with a sell date of {0} increase in quality by 2")
    @ValueSource(ints = [6, 7, 8, 9, 10])
    fun `update of non-expired item with sell date between 5 and 10 days increases its value by 2`(sellIn: Int) {
        val item = Item("Backstage passes to a TAFKAL80ETC concert", sellIn, 20)

        val updatedItem = strategy(item)

        assertThat(updatedItem.quality).isEqualTo(item.quality + 2)
    }

    @Test
    fun `update of non-expired item with sell date of more than 10 days increases its value by 1`() {
        val item = Item("Backstage passes to a TAFKAL80ETC concert", 24, 20)

        val updatedItem = strategy(item)

        assertThat(updatedItem.quality).isEqualTo(item.quality + 1)
    }

    @Test
    fun `update of item decreases its sell date with 1`() {
        val item = Item("Backstage passes to a TAFKAL80ETC concert", 0, 0)

        val updatedItem = strategy(item)

        assertThat(updatedItem.sellIn).isEqualTo(item.sellIn - 1)
    }
}
