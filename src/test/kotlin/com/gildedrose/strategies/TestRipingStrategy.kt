package com.gildedrose.strategies

import com.gildedrose.item.Item
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class TestRipingStrategy {

    private val strategy = ::ripingStrategy

    @Test
    fun `update of item with non-expired sell date increases its quality by 1`() {
        val item = Item("Aged Brie", 2, 0)

        val updatedItem = strategy(item)

        assertThat(updatedItem.quality).isEqualTo(item.quality + 1)
    }

    @Test
    fun `update of item with expired sell date increases its quality with 2`() {
        val item = Item("Aged Brie", 0, 0)

        val updatedItem = strategy(item)

        assertThat(updatedItem.quality).isEqualTo(item.quality + 2)
    }

    @Test
    fun `update of item never increases its quality beyond 50`() {
        val item = Item("Aged Brie", 2, 50)

        val updatedItem = strategy(item)

        assertThat(updatedItem.quality).isEqualTo(item.quality)
    }

    @Test
    fun `update of item with non-expired sell date and negative quality increases its quality by 1`() {
        val item = Item("Aged Brie", 2, -1)

        val updatedItem = strategy(item)

        assertThat(updatedItem.quality).isEqualTo(item.quality + 1)
    }
}
