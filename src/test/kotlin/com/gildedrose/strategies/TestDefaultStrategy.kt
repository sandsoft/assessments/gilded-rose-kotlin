package com.gildedrose.strategies

import com.gildedrose.item.Item
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class TestDefaultStrategy {

    private val strategy = ::defaultStrategy

    @Test
    fun `update of item with non-expired sell date decreases its quality with 1`() {
        val item = Item("+5 Dexterity Vest", 10, 20)

        val updatedItem = strategy(item)

        assertThat(updatedItem.quality).isEqualTo(item.quality - 1)
    }

    @Test
    fun `update of item with expired sell date decreases its quality with 2`() {
        val item = Item("+5 Dexterity Vest", -1, 20)

        val updatedItem = strategy(item)

        assertThat(updatedItem.quality).isEqualTo(item.quality - 2)
    }

    @Test
    fun `update of item decreases its sell date with 1`() {
        val item = Item("+5 Dexterity Vest", 10, 20)

        val updatedItem = strategy(item)

        assertThat(updatedItem.sellIn).isEqualTo(item.sellIn - 1)
    }

    @Test
    fun `update of item with non-expired sell date never makes its quality negative`() {
        val item = Item("+5 Dexterity Vest", 1, 0)

        val updatedItem = strategy(item)

        assertThat(updatedItem.quality).isEqualTo(item.quality)
    }

    @Test
    fun `update of item with an expired sell date never makes its quality negative`() {
        val item = Item("+5 Dexterity Vest", 0, 0)

        val updatedItem = strategy(item)

        assertThat(updatedItem.quality).isEqualTo(item.quality)
    }
}
