package com.gildedrose.strategies

import com.gildedrose.item.Item
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class TestConjuredStrategy {

    private val strategy = ::conjuredStrategy

    @Test
    fun `update of item with non-expired sell date decreases its quality with 2`() {
        val item = Item("+5 Dexterity Vest", 10, 20)

        val updatedItem = strategy(item)

        assertThat(updatedItem.quality).isEqualTo(item.quality - 2)
    }

    @Test
    fun `update of item with expired sell date decreases its quality with 4`() {
        val item = Item("+5 Dexterity Vest", -1, 20)

        val updatedItem = strategy(item)

        assertThat(updatedItem.quality).isEqualTo(item.quality - 4)
    }

    @Test
    fun `update of item decreases its sell date with 1`() {
        val item = Item("+5 Dexterity Vest", 10, 20)

        val updatedItem = strategy(item)

        assertThat(updatedItem.sellIn).isEqualTo(item.sellIn - 1)
    }

    @Test
    fun `update of item with non-expired sell date never makes its quality negative`() {
        val item = Item("+5 Dexterity Vest", 1, 1)

        val updatedItem = strategy(item)

        assertThat(updatedItem.quality).isEqualTo(0)
    }

    @Test
    fun `update of item with an expired sell date never makes its quality negative`() {
        val item = Item("+5 Dexterity Vest", 0, 1)

        val updatedItem = strategy(item)

        assertThat(updatedItem.quality).isEqualTo(0)
    }
}
