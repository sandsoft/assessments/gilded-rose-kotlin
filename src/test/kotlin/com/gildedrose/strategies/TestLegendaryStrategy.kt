package com.gildedrose.strategies

import com.gildedrose.item.Item
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class TestLegendaryStrategy {

    private val strategy = ::legendaryStrategy

    @Test
    fun `update of legendary item does not decrease its quality`() {
        val item = Item("Sulfuras, Hand of Ragnaros", 0, 80)

        val updatedItem = strategy(item)

        assertThat(updatedItem.quality).isEqualTo(item.quality)
    }

    @Test
    fun `update of legendary item does not decrease its sell date`() {
        val item = Item("Sulfuras, Hand of Ragnaros", -1, 80)

        val updatedItem = strategy(item)

        assertThat(updatedItem.sellIn).isEqualTo(updatedItem.sellIn)
    }
}
