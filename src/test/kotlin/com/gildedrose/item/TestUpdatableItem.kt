package com.gildedrose.item

import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class TestUpdatableItem {

    private val item = Item("+5 Dexterity Vest", 10, 20)

    private val strategyMock = mockk<(Item) -> Item>()
    private val itemSlot = slot<Item>()

    @Test
    fun `update of an updatable item correctly invokes its update strategy`() {
        val updatableItem = UpdatableItem(item = item, updateStrategy = strategyMock)
        every { strategyMock(capture(itemSlot)) } returns item

        updatableItem.update()

        verify(exactly = 1) {
            strategyMock(any())
        }
        assertThat(item).usingRecursiveComparison()
            .isEqualTo(itemSlot.captured)
    }
}
