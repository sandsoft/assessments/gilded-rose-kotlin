package com.gildedrose.item

import com.gildedrose.strategies.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class TestItemFactory {

    @Test
    fun `getUpdatableItem returns item with legendary update strategy if name contains 'Sulfuras'`() {
        val legendaryItem = Item("Sulfuras, Hand of Ragnaros", 0, 80)

        val item = getUpdatableItem(item = legendaryItem)

        assertThat(item.updateStrategy).isEqualTo(::legendaryStrategy)
    }

    @Test
    fun `getUpdatableItem returns item with riping strategy if name contains 'Aged Brie'`() {
        val ripingItem = Item("Aged Brie", 0, 80)

        val item = getUpdatableItem(item = ripingItem)

        assertThat(item.updateStrategy).isEqualTo(::ripingStrategy)
    }

    @Test
    fun `getUpdatableItem returns item with ticket strategy if name contains 'Backstage passes'`() {
        val ticketItem = Item("Backstage passes to a TAFKAL80ETC concert", 15, 20)

        val item = getUpdatableItem(item = ticketItem)

        assertThat(item.updateStrategy).isEqualTo(::ticketStrategy)
    }

    @Test
    fun `getUpdatableItem returns item with conjured strategy if name contains 'Conjured'`() {
        val ticketItem = Item("Conjured Mana Cake", 3, 6)

        val item = getUpdatableItem(item = ticketItem)

        assertThat(item.updateStrategy).isEqualTo(::conjuredStrategy)
    }

    @Test
    fun `getUpdatableItem returns item with default strategy in default case`() {
        val defaultItem = Item("Elixir of the Mongoose", 5, 7)

        val item = getUpdatableItem(item = defaultItem)

        assertThat(item.updateStrategy).isEqualTo(::defaultStrategy)
    }
}
