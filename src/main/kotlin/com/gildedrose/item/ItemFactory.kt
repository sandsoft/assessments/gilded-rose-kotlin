package com.gildedrose.item

import com.gildedrose.strategies.*

fun getUpdatableItem(item: Item): UpdatableItem {
    val updateStrategy = with(item.name) {
        when {
            contains("Sulfuras") -> ::legendaryStrategy
            contains("Backstage passes") -> ::ticketStrategy
            contains("Aged Brie") -> ::ripingStrategy
            contains("Conjured") -> ::conjuredStrategy
            else -> ::defaultStrategy
        }
    }
    return UpdatableItem(
        item = item.copy(),
        updateStrategy = updateStrategy
    )
}
