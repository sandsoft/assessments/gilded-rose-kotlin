package com.gildedrose.item

class UpdatableItem(
    private val item: Item,
    val updateStrategy: (Item) -> Item
) {
    val name = item.name
    val sellIn = item.sellIn
    val quality = item.quality

    fun update() = UpdatableItem(
        item = updateStrategy(item.copy()),
        updateStrategy = updateStrategy,
    )

    override fun toString(): String {
        return "$name, $sellIn, $quality"
    }
}
