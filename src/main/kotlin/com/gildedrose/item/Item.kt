package com.gildedrose.item

open class Item(var name: String, var sellIn: Int, var quality: Int) {
    override fun toString(): String {
        return this.name + ", " + this.sellIn + ", " + this.quality
    }
}

fun Item.copy(): Item {
    return Item(
        name = this.name,
        sellIn = this.sellIn,
        quality = this.quality,
    )
}
