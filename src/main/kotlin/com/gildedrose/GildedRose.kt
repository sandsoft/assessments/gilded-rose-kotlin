package com.gildedrose

import com.gildedrose.item.UpdatableItem

class GildedRose(val items: List<UpdatableItem>) {

    fun update(): GildedRose {
        return GildedRose(
            items = items.map { it.update() }
        )
    }
}
