package com.gildedrose.strategies

import com.gildedrose.item.Item
import com.gildedrose.item.copy

fun legendaryStrategy(i: Item) = i.copy()
