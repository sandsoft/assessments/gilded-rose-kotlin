package com.gildedrose.strategies

import com.gildedrose.item.Item
import kotlin.math.max

fun defaultStrategy(i: Item): Item {
    val newSellIn = i.sellIn - 1
    val newQuality = max(
        0,
        when (newSellIn) {
            in Int.MIN_VALUE until 0 -> i.quality - 2
            else -> i.quality - 1
        }
    )
    return Item(
        name = i.name,
        sellIn = newSellIn,
        quality = newQuality
    )
}
