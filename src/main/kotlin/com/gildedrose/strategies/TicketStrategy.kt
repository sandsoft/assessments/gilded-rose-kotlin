package com.gildedrose.strategies

import com.gildedrose.item.Item
import kotlin.math.min

fun ticketStrategy(i: Item): Item {
    val newSellIn = i.sellIn - 1
    val newQuality = min(
        50,
        when (newSellIn) {
            in 10..Int.MAX_VALUE -> i.quality + 1
            in 5 until 10 -> i.quality + 2
            in 0 until 5 -> i.quality + 3
            else -> 0
        }
    )
    return Item(
        name = i.name,
        sellIn = newSellIn,
        quality = newQuality,
    )
}
